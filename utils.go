package utils

func Contains(l []string, s string) bool {
	for _, v := range l {
		if v == s {
			return true
		}
	}
	return false
}

func InSliceInt(s []int, n int) bool  {
	for _, v := range s {
		if v == n {
			return true
		}
	}
	return false
}